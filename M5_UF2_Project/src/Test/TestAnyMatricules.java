package Test;

import static org.junit.Assert.*;
import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
	
@RunWith(Parameterized.class)
public class TestAnyMatricules {


	private String matricula;
	private int venuts;
	private int any;
	
	public TestAnyMatricules (String matricula, int venuts, int any) {
		this.matricula = matricula;
		this.venuts= venuts;
		this.any = any;
	
	}
	
	@Parameters
	public static Collection<Object[]> numeros() {
		return Arrays.asList(new Object[][] {
			{"1500FF", 133333, 1994},
			{"1598PB", 133333, 2006},
			{"0000A�", 133333, 1899},
			{"6782BB", 133333, 1990},
			{"1234EM", 133333, 1899},
			{"9999JS", 133333, 2000},
			{"6666OZ", 133333, 1899},
			{"4598LI", 133333, 1899},
			{"0000ZZ", 133333, 2020},
			{"0009PU", 133333, 1899},
			{"5555QK", 133333, 1899},
			{"4555LM", 133333, 2002},
			{"9594DD", 133333, 1993},
			{"9463GK", 133333, 1996},
			{"1981SS", 133333, 2010},
			{"1986XX", 133333, 2016},
		});
	}
	
	@Test
	public void testAnyMatricula() {
		int res = Codes.Matricules.anyMatricula(matricula,  venuts);
		assertEquals(any,  res);
	}
}