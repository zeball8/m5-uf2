package Codes;

import java.util.Scanner;

/**
 * <h2>clase Matricules</h2>
 * <p>Programa que calcula l'any de matriculacio d'un vehicle i ens diu si pot entrar a la zona de baixes emisions de Barcelona.</p>
 * Institut Sabadell
 * CFGS DAM M05
 * UF2
 * Projecte
 * Busca informacion de l'institut en <a href="https://agora.xtec.cat/ies-sabadell/">IES Sabadell</a>
 * @see <a href="https://www.zbe.barcelona/">ZBE Rondes de Barcelona</a>
 * @version 2-2020
 * @author Isaac Garcia
 * @since 14-02-2020
 */

public class Matricules {
	static Scanner sc = new Scanner(System.in);
	
	/**
	 * El metode main nomes s'encarrega de cridar al metodo principal.
	 */
	public static void main(String[] args) {
		Matricules zbe = new Matricules();
		zbe.master();
	}

	/**
	 * Metode principal, s'introdueixen les dades solicitades per a realitzar la consulta i es criden a la resta de metodes.
	 */
	public void master() {
		int i=0;
		int venuts=133333;
		int any;
		String matricula;
		char tipus;
		//System.out.print("Quantes matricules vols comprovar?: ");
		int n = sc.nextInt();
		sc.nextLine();
		
		while (i<n) {
			//System.out.print("Introduu tipus de combustible (Diesel [B] - Benzina [E]): ");
			String combustible=sc.nextLine().toUpperCase();  
			tipus=combustible.charAt(0);  
			matricula=demanaMatricula();
			any=anyMatricula(matricula, venuts);
			ZBE_si_no(any, tipus);
			i++;
		}
	}
	
	/**
	 * Metode que converteix la matricula introduida per l'usuari a un vector de caracters
	 *  @return <b>matricula</b>, Retornem la matricula  introduida dins d'un vectorr <i>char</i>.
	 */
	public static String demanaMatricula() {
		//System.out.println("Introdueixi la matricula, 4 nmeros i 2 lletres sense espais (Les vocals i les lletres Q i  estan excloses): ");
		String matricula=sc.nextLine().toUpperCase();  
		return matricula;
	}
	
	/**
	 * Metode que calcula l'any de matriculacio.
	 *  @param matricula Vector de caracters amb la matricula.
	 *  @param venuts nombre de vehicles venuts anualment.
	 *  @return <b>any</b>, retornem l'any de matriculacio.
	 */
	public static int anyMatricula(String matricula, int venuts) {
		int any=1899;
		int c1;int c2;
		
		if ((matricula.charAt(4)=='A'||matricula.charAt(4)=='E'||matricula.charAt(4)=='I'||matricula.charAt(4)=='O'||matricula.charAt(4)=='U'||matricula.charAt(4)==164||matricula.charAt(4)=='Q')||
			(matricula.charAt(5)=='A'||matricula.charAt(5)=='E'||matricula.charAt(5)=='I'||matricula.charAt(5)=='O'||matricula.charAt(5)=='U'||matricula.charAt(5)==164||matricula.charAt(5)=='Q')) {
			return any;
		} else {
			c1=conversor(matricula.charAt(4));
			c2=conversor(matricula.charAt(5));
			if (c1==1) {
				return any=((c2*10000)/venuts)+1990;
			} else {
				return any=((((c1-1)*200000)+(c2*10000))/venuts)+1990;
			}
		}
	}
	
	/**
	 * Metode que mostra per pantalla el resultat obtingut.
	 *  @param any L'any de matriuclacio obtingut.
	 *  @param tipus El tipus de combustible utilitzat.
	 */
	public static void ZBE_si_no(int any, char tipus) {
		if (any==1899) {
			System.out.println("ERR");
		} else if (tipus=='B'&& any<2006){
			System.out.println(any+" NO");
		} else if (tipus=='B'&& any>=2006){
			System.out.println(any+" SI");
		} else if (tipus=='E'&& any<2000){
			System.out.println(any+" NO");
		} else if (tipus=='E'&& any>=2000){
			System.out.println(any+" SI");
		}
	}
	
	/**
	 * Funcio que proporciona a cada lletra un valor <i>integer</i> que servira per a poder aplicar la formula de l'any.
	 *  @param lletra El caracter de la matricula.
	 *  @return <b>n</b>, retornem el valor de la lletra.
	 */
	public static int conversor(char lletra) {
		int n=0;
		switch (lletra) {
			case 'B':
				n=1;break;
			case 'C':
				n=2;break;
			case 'D':
				n=3;break;
			case 'F':
				n=4;break;
			case 'G':
				n=5; break;
			case 'H':
				n=6; break;
			case 'J':
				n=7; break;
			case 'K': 
				n=8; break;
			case 'L':
				n=9; break;
			case 'M':
				n=10; break;
			case 'N':
				n=11; break;
			case 'P': 
				n=12; break;
			case 'R':
				n=13; break;
			case 'S':
				n=14; break;
			case 'T':
				n=15; break;
			case 'V': 
				n=16; break;
			case 'W':
				n=17; break;
			case 'X':
				n=18; break;
			case 'Y':
				n=19; break;
			case 'Z': 
				n=20; break;
		}
		return n;
	}
}
